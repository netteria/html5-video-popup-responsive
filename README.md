# Html5 video popup responsive and youtube video modal (jQuery and css)
A simple script jquery embedding a video clip in a modal window. Video is responsive.




More on and DEMO [HTML5 responsive popup video](https://netteria.net/html5-video-popup-jquery/105/)
Create by [Netteria.NET](https://netteria.net) 2017.

Donate

If this script was useful, you can buy me coffee;) [Donate via Paypal](https://paypal.me/forcoffee897?locale.x=pl_PL)
Thanks!

Youtube video: https://codepen.io/netteria/pen/dLWxJJ
